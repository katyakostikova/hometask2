import { controls } from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  return new Promise(resolve => {
    let keysPressed = {};
    const firstFighterMaxHealth = firstFighter.health;
    const secondFighterMaxHealth = secondFighter.health;
    let isFirstFighterCritTimedOut = false;
    let isSecondFighterCritTimedOut = false;

    document.addEventListener("keydown", event => {
      keysPressed[event.code] = true;

      if (
        keysPressed[controls.PlayerOneAttack] &&
        !keysPressed[controls.PlayerTwoBlock] &&
        !keysPressed[controls.PlayerOneBlock] 
      ) {
        const damage = getDamage(firstFighter, secondFighter);
        secondFighter.health -= damage;

        const hpPercentage =
          calculateHpPercentage(secondFighter.health, secondFighterMaxHealth) + "%";
        changeHpBar("right-fighter-indicator", hpPercentage);

      } if (
        keysPressed[controls.PlayerTwoAttack] &&
        !keysPressed[controls.PlayerOneBlock] &&
        !keysPressed[controls.PlayerTwoBlock]
      ) {
        const damage = getDamage(secondFighter, firstFighter);
        firstFighter.health -= damage;

        const hpPercentage =
          calculateHpPercentage(firstFighter.health, firstFighterMaxHealth) + "%";
        changeHpBar("left-fighter-indicator", hpPercentage);

      } 
      //crit damage logic
      if (!isFirstFighterCritTimedOut) {
        if (
          keysPressed[controls.PlayerOneCriticalHitCombination[0]] &&
          keysPressed[controls.PlayerOneCriticalHitCombination[1]] &&
          keysPressed[controls.PlayerOneCriticalHitCombination[2]] &&
          !keysPressed[controls.PlayerOneBlock]
        ) {
          const damage = getCritHitPower(firstFighter);
          secondFighter.health -= damage;

          const hpPercentage =
            calculateHpPercentage(secondFighter.health, secondFighterMaxHealth) + "%";
          changeHpBar("right-fighter-indicator", hpPercentage);

          isFirstFighterCritTimedOut = true;
          setTimeout(() => {
            isFirstFighterCritTimedOut = false;
          }, 10000);
        }
      }

      if (!isSecondFighterCritTimedOut) {
        if (
          keysPressed[controls.PlayerTwoCriticalHitCombination[0]] &&
          keysPressed[controls.PlayerTwoCriticalHitCombination[1]] &&
          keysPressed[controls.PlayerTwoCriticalHitCombination[2]] &&
          !keysPressed[controls.PlayerTwoBlock]
        ) {
          const damage = getCritHitPower(secondFighter);
          firstFighter.health -= damage;

          const hpPercentage =
            calculateHpPercentage(firstFighter.health, firstFighterMaxHealth) + "%";
          changeHpBar("left-fighter-indicator", hpPercentage);

          isSecondFighterCritTimedOut = true;
          setTimeout(() => {
            isSecondFighterCritTimedOut = false;
          }, 10000);
        }
      }

      //winner logic
      if (firstFighter.health <= 0) resolve(secondFighter);
      else if (secondFighter.health <= 0) resolve(firstFighter);
    });

    //remove all cached buttons
    document.addEventListener("keyup", event => {
      keysPressed[event.code] = false
    });
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  if (blockPower > hitPower) return 0;
  return hitPower - blockPower;
}

export function getCritHitPower(fighter) {
  const power = fighter.attack * 2;
  return power;
}

export function getHitPower(fighter) {
  const critDamageChance = generateRandomNum();
  const power = fighter.attack * critDamageChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = generateRandomNum();
  const power = fighter.defense * dodgeChance;
  return power;
}

function changeHpBar(hpBarId, percentage) {
  const hpBar = document.getElementById(hpBarId);
  hpBar.style.width = percentage;
}

function calculateHpPercentage(tempHp, maxHp) {
  return (tempHp / maxHp) * 100;
}

function generateRandomNum() {
  return Math.random() + 1;
}
