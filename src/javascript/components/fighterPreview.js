import { createElement } from "../helpers/domHelper";
import { fighters } from "../helpers/mockData";

export function createFighterPreview(fighter, position) {
  const positionClassName =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const { name, health, attack, defense } = fighter;
    const fighterName = createElement({
      tagName: "h1",
      className: "fighter-preview__name",
    });
    fighterName.innerText = name;
    const fighterImage = createFighterImage(fighter);
    const fighterStats = createElement({
      tagName: "p",
      className: "fighter-preview__stats",
    });
    fighterStats.innerText = `⚔️Attack: ${attack}\n 🛡️Defence: ${defense}\n ❤️Health: ${health}`;
    fighterElement.append(fighterName, fighterImage, fighterStats);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-preview___img",
    attributes,
  });

  return imgElement;
}
