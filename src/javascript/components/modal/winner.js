import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  showModal({
    title: `Winner: ${fighter.name}`,
    bodyElement: `Current health: ${Math.floor(fighter.health)}`,
    onClose: () => {
      document.location.reload();
    }
  })
}
